#!/bin/bash
#
# Export source and receiver coordinates to VTK file for visualization
# with Paraview.
#
# Francois Lavoue, 16 July 2019  
# Last updated FL, 19 July 2019


#-- user parameters

dir_in="results/output_run_simulation_in_simple_synthetic_model_v8a_2-horizontal-layers"

file_in="$dir_in/acqui"   # (acqui or acqui_topo)

file_ou1="$dir_in/vtk_files/src.vtk"
file_ou2="$dir_in/vtk_files/rec.vtk"

isrc=1   # 0 -> export all sources, i -> export source nb i

#-- end user parameters


#-- export source(s) to VTK
echo "# vtk DataFile Version 2.0" >$file_ou1
echo "Source VTK file" >>$file_ou1
echo "ASCII" >>$file_ou1
echo "DATASET POLYDATA" >>$file_ou1

if [ $isrc -eq 0 ]; then
   # export all sources
   nsrc=$(awk <$file_in '{if($6==0) {print $0}}' | wc | awk '{print $1}')
   echo "POINTS      $nsrc float" >>$file_ou1
   awk <$file_in '{if($6==0) {printf "%12.3f  %12.3f  %12.3f\n", $2, $3, $1}}' >>$file_ou1
else
   # export source nb i
   echo "POINTS      1 float" >>$file_ou1
   awk <$file_in '{if($6==0) {print $0}}' | \
   awk           'NR=='$isrc' {printf "%12.3f  %12.3f  %12.3f\n", $2, $3, $1}' >>$file_ou1
fi


#-- export receivers to VTK
# NB: we export all receivers for now (not ideal in case of several sources)
echo "# vtk DataFile Version 2.0" >$file_ou2
echo "Receiver VTK file" >>$file_ou2
echo "ASCII" >>$file_ou2
echo "DATASET POLYDATA" >>$file_ou2

nrec=$(awk <$file_in '{if($6==1) {print $0}}' | wc | awk '{print $1}')
echo "POINTS      $nrec float" >>$file_ou2
awk <$file_in '{if($6==1) {printf "%12.3f  %12.3f  %12.3f\n", $2, $3, $1}}' >>$file_ou2

