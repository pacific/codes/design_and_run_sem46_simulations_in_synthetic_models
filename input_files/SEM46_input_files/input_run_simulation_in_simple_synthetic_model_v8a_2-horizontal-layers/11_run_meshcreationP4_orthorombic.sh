#!/bin/bash
#
# Run the SEM46 mesher (meshcreationP4_orthorombic).
#
# Notes:
#
# - This script is written for the dahu cluster (UGA) where it must be
#   submitted using
#     oarsub -S ./11_run_meshcreationP4_orthorombic.sh
#   (do not forget the "./")
#
# - You may adapt it to your system (esp. $path2sem46 below).
#
# Francois Lavoue, 29 May 2019

path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_dev_FL/bin"
path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk/bin"
#NB: /home/lavoue/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk -> /home/brossier/SEM46_FOR_USERS/SEM46_trunk/bin

bin_exec="meshcreationP4_orthorombic_Q"   # "meshcreationP4_orthorombic" or "meshcreationP4_orthorombic_Q"

## NB: mesher is run in sequential, so we need only 1 core,
#OAR -l core=1,walltime=00:10:00
##     but we might need the entire node memory for large meshes.
###OAR -l /nodes=1/core=32,walltime=01:00:00
#OAR --project pr-pacific
#OAR -n SEM46
#OAR -t devel
# (disable devel mode for quicker launch if devel nodes are not available)

# load environment
ulimit -s unlimited
source ~/.nix-profile/bin/iccvars.sh -arch intel64
source ~/.nix-profile/compilers_and_libraries_2017.4.196/linux/mpi/bin64/mpivars.sh

cd $OAR_WORKDIR
cat $OAR_NODE_FILE

# OPENMP and MKL TUNING
export MKL_NUM_THREADS=1
export OMP_NUM_THREADS=1

mkdir -p job_info

# launch the preprocessing step (always in sequential) to build the mesh 
$path2sem46/$bin_exec >job_info/log11_meshcreation.out 2>job_info/log11_meshcreation.err

