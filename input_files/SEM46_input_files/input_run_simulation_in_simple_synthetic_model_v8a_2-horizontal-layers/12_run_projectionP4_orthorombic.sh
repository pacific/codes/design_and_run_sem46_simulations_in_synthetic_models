#!/bin/bash
#
# Run SEM46 projection step (projectionP4_orthorombic) to build GLL grids
# from input Cartesian grids.
#
# Notes:
#
# - In case of topography, you need to run this again with "working_option=3"
#   in projection_input_namelist in order to project the acquisition.
#
# - This script is written for the dahu cluster (UGA) where it must be
#   submitted using
#     oarsub -S ./12_run_projectionP4_orthorombic.sh
#   (do not forget the "./")
#
# - You may adapt it to your system (esp. $path2sem46 below).
#
# Francois Lavoue, 29 May 2019

path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_dev_FL/bin"
path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk/bin"
#NB: /home/lavoue/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk -> /home/brossier/SEM46_FOR_USERS/SEM46_trunk/bin

bin_exec="projectionP4_orthorombic_Q"   # "projectionP4_orthorombic" or "projectionP4_orthorombic_Q"

##OAR -l nodes=1/core=4,walltime=00:30:00
## or, if more memory is needed:
#OAR -l nodes=1/core=32,walltime=00:30:00
#OAR --project pr-pacific
#OAR -n SEM46
#OAR -t devel
# (if run on 1 core, disable devel mode for quicker launch if devel nodes are not available)

# number of cores
nbcores=`cat $OAR_NODE_FILE | wc -l`

# redefine exact nb of MPI processes in case we requested too many cores (e.g. if more memory is needed)
nbcores=4

# OPENMP and MKL TUNING
export MKL_NUM_THREADS=1
export OMP_NUM_THREADS=1

# load environment
ulimit -s unlimited
source ~/.nix-profile/bin/iccvars.sh -arch intel64
source ~/.nix-profile/compilers_and_libraries_2017.4.196/linux/mpi/bin64/mpivars.sh

# go to working dir
cd $OAR_WORKDIR
cat $OAR_NODE_FILE

mkdir -p job_info

# launch the projection step to build gll grids from Cartesian grids 
mpirun -n $nbcores $path2sem46/$bin_exec >job_info/log12_projection.out 2>job_info/log12_projection.err

