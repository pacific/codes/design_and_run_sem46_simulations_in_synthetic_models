#!/bin/bash
#
# Run SEM46 simulation (sem46P4_orthorombic).
#
# Notes:
#
# - This script is written for the dahu cluster (UGA) where it must be
#   submitted using
#     oarsub -S ./13_run_sem46P4_orthorombic.sh
#   (do not forget the "./")
#
# - You may adapt it to your system (esp. $path2sem46 below), and of
#   course to your simulation (nb of requested nodes and cores).
#
# Francois Lavoue, 29 May 2019

path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_dev_FL/bin"
path2sem46="$HOME/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk/bin"
#NB: /home/lavoue/git/SEISCOPE/SVN_CODES/SEM46/SEM46_trunk -> /home/brossier/SEM46_FOR_USERS/SEM46_trunk/bin

bin_exec="sem46P4_orthorombic_Q"   # "sem46P4_orthorombic" or "sem46P4_orthorombic_Q"

#OAR -l /nodes=1/core=4,walltime=24:00:00
###OAR -t devel
#OAR --project pr-pacific
#OAR -n SEM46
###OAR --notify exec:/usr/local/bin/sendmail.sh

# load environment
source /applis/site/nix.sh
source ~/.nix-profile/bin/iccvars.sh -arch intel64
source ~/.nix-profile/compilers_and_libraries_2017.4.196/linux/mpi/bin64/mpivars.sh
ulimit -s unlimited

cd $OAR_WORKDIR
# Number of cores
nbcores=`cat $OAR_NODE_FILE|wc -l`
# Number of nodes
nbnodes=`cat $OAR_NODE_FILE|sort|uniq|wc -l`
# Name of the first node
firstnode=`head -1 $OAR_NODE_FILE`
# Number of cores allocated on the first node (it is the same on all the nodes)
pernode=`grep $firstnode $OAR_NODE_FILE|wc -l`

echo $nbnodes
echo $nbcores

cat $OAR_NODE_FILE
export MKL_NUM_THREADS=1
export OMP_NUM_THREADS=1

# redefine nb of MPI processes in case it is not a multiple of nb of cores per node
#nbcores=190

# launch the application
mkdir -p job_info
mpiexec.hydra -genvall -f $OAR_NODE_FILE -bootstrap-exec oarsh -perhost $pernode -n $nbcores $path2sem46/$bin_exec \
   >job_info/log13_sem46.out 2>job_info/log13_sem46.err

