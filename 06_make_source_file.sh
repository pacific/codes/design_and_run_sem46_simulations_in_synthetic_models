#!/bin/bash
#
# Generate a Ricker source file for SEM46 based on the time step and
# duration defined in sem_input_namelist.
#
# Francois Lavoue, 24 June 2019
# Last updated FL, 16 July 2019


#-- user parameters

dir0="input_files/SEM46_input_files"

dir_in="$dir0/input_run_simulation_in_simple_synthetic_model_v8a_2-horizontal-layers"
file_in="$dir_in/sem_input_namelist"

dir_ou="$dir0/sources"

source_type="ricker"   # "ricker" or "gaussian" (not implemented yet)

f0=5.0   # peak frequency (in Hz)

#-- end user parameters


echo
echo "Make source file in"
echo "  $dir_ou"
mkdir -p $dir_ou
echo

#-- read time step and duration defined in sem_input_namelist
nt=$(grep '&FORWARD_PARAM' $file_in | awk '{print $3}' | sed 's/total_nb_time_step=//g' | sed 's/,//g')
dt=$(grep '&FORWARD_PARAM' $file_in | awk '{print $2}' | sed 's/dt=//g' | sed 's/,//g')


#-- make source signal
if [ $source_type == "ricker" ]; then

   #-- init.
   file_ou="$dir_ou/$(echo "$f0 $nt $dt" | awk '{printf "fricker_%gHz_nt%i_dt%gs.bin", $1, $2, $3}')"
   rm $file_ou

   #-- run Seiscope Fortran program that computes Ricker and its derivative/primitive (should be compiled first)
./scripts/make_ricker/ricker <<EOF
$nt, $dt, $f0
EOF
   # FL: this program computes a Ricker and its derivative/primitive,
   # and it turns out that the 2nd primitive of a Ricker is a Gaussian
   # that could be used for simulations too. But the frickerpp created
   # here seems to have a static frequency component (its spectrum is
   # half a Gaussian centred at 0 Hz), which I'm not sure to understand,
   # so I leave it for later for now...

   #-- rename/remove outputs
   rm frickerd frickerp frickerpp
   mv fricker $file_ou

else
   echo "Error: source type '$source_type' not implemented."
fi

echo

##-- go to output dir and link file to generic fsource
# (useful only if output dir is a run directory)
#cwd=$(pwd)
#cd $dir_ou
#   echo "ln -sf $(basename $file_ou) ./fsource"
#         ln -sf $(basename $file_ou) ./fsource
#cd $cwd

echo

