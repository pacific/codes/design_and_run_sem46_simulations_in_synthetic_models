#!/bin/bash
#
# Check domain decomposition for SEM46, based on the nb of elements
# specified in mesh_name_list.
#
# Francois Lavoue, 24 June 2019
# Last updated FL, 9 Jan 2020

#-- user parameters

dir0="input_files/SEM46_input_files"

dir_in="$dir0/input_run_simulation_in_simple_synthetic_model_v8a_2-horizontal-layers"
file_in="$dir_in/mesh_input_namelist"

ntest=7   # nb of domain decomposition settings to test (ncores=1 to ntest)

#-- end user parameters


echo
echo "Check domain decomposition for SEM46 in"
echo "  $dir_in"
echo

#-- read nb of elements in mesh_input_namelist
nel_z=$(grep '&MESH_HOMO' $file_in | awk '{print $2}' | sed 's/Nb_elementZ=//g' | sed 's/,//g')
nel_x=$(grep '&MESH_HOMO' $file_in | awk '{print $3}' | sed 's/Nb_elementX=//g' | sed 's/,//g')
nel_y=$(grep '&MESH_HOMO' $file_in | awk '{print $4}' | sed 's/Nb_elementY=//g' | sed 's/,//g')
nsponge=$(grep '&MESH_INFO' $file_in | awk '{print $6}' | sed 's/sponge_length=//g' | sed 's/,//g')

echo "nel_z, nel_x, nel_y = $nel_z, $nel_x, $nel_y"
echo "nsponge = $nsponge"

echo

# Test domain decomposition settings

for (( itest=1; itest<=$ntest; itest++)); do
    echo "-----"
    ndz=$itest
    ndx=$itest
    ndy=$itest
    ncores=$(($ndz*$ndx*$ndy))
    #nnodes_i=$(($ncores/32))
    nnodes_f=$( echo "$ncores/32" | bc -l | awk '{printf "%.2f", $1}')
    echo "ncores = ndz x ndx x ndy = $ndz x $ndx x $ndy = $ncores = $nnodes_f 32-core nodes"

    echo

    crit_z=$(echo "($nel_z+  $nsponge)/$ndz" | bc -l | awk '{printf "%.2f", $1}')
    crit_x=$(echo "($nel_x+2*$nsponge)/$ndx" | bc -l | awk '{printf "%.2f", $1}')
    crit_y=$(echo "($nel_y+2*$nsponge)/$ndy" | bc -l | awk '{printf "%.2f", $1}')

    echo "(nel_z +   nsponge)/nd_z = $crit_z   (must be > nsponge = $nsponge, except if ndz = 1)"
    echo "(nel_x + 2*nsponge)/nd_x = $crit_x   (must be > nsponge = $nsponge)"
    echo "(nel_y + 2*nsponge)/nd_y = $crit_y   (must be > nsponge = $nsponge)"

    echo

    if [ $(echo "$crit_z<$nsponge" | bc -l) -eq 1 ] && \
       [ $(echo "$crit_x<$nsponge" | bc -l) -eq 1 ] && \
       [ $(echo "$crit_y<$nsponge" | bc -l) -eq 1 ]; then
       break   # stop here as more cores won't be possible
    fi
done

echo "You may now edit sem_input_namelist accordingly."

