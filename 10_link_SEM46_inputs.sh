#!/bin/bash

function USAGE {
echo
echo "Usage: `basename $0` <dir_in>"
echo
echo "  Link inputs for SEM46 from a given directory."
echo
echo "Francois Lavoue, 26 June 2019"
echo
}   # end function usage

#-- if not enough input argument is given, display usage.
if [ $# -lt 1 ]; then USAGE; exit; fi

dir_in=$1

ln -sf $dir_in/11_run_meshcreationP4_orthorombic.sh .
ln -sf $dir_in/12_run_projectionP4_orthorombic.sh .
ln -sf $dir_in/13_run_sem46P4_orthorombic*.sh .

ln -sf $dir_in/acqui .
ln -sf $dir_in/fsource .
ln -sf $dir_in/exotic_QC_input_namelist .
ln -sf $dir_in/mesh_input_namelist .
ln -sf $dir_in/projection_input_namelist .
ln -sf $dir_in/sem_input_namelist .
ln -sf $dir_in/topo .

if [ -f $dir_in/acqui_id ]; then ln -sf $dir_in/acqui_id . ; fi
if [ -f $dir_in/extra_mesh_info.in ]; then ln -sf $dir_in/extra_mesh_info.in . ; fi
if [ -f $dir_in/vp_cart ]; then ln -sf $dir_in/*_cart . ; fi

