#!/bin/bash
#
# Build a library of synthetic models with simple geometries, as
# suggested by Dan and Richard (2D version).
#
# Francois Lavoue, 21 May 2019

mkdir -p job_info
nohup python ./scripts/build_simple_synthetic_model_2D.py >job_info/log_01.out 2>job_info/log_01.err &

