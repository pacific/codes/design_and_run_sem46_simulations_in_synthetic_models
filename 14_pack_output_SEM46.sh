#!/bin/bash

function USAGE {
echo
echo "Move and organize the outputs of a SEM46 simulation into a given folder."
echo
echo "  Usage: `basename $0` <dir_out>"
echo
echo "Notes:"
echo
echo "- this script may output error messages for non-existing files,"
echo "  you may ignore most of them."
echo
echo "- organization is mostly a subjective matter... you may change"
echo "  the way this is done here at your convenience."
echo
echo "Francois Lavoue, 5 June 2019"
echo "Last updated FL, 15 July 2019"
echo
}   # end function usage

#-- if not enough input argument is given, display usage.
if [ $# -lt 1 ]; then USAGE; exit; fi

#-- define and check output dir
dir_ou=$1
if [ -d $dir_ou ]; then
   echo "Error: output dir already exists. Delete it first."
   exit
fi

# deleted unwanted outputs
rm OAR*
rm fort.*

# make output dir and subdirs
mkdir -p $dir_ou
mkdir $dir_ou/figs
mkdir $dir_ou/model_cart
mkdir $dir_ou/model_gll
mkdir $dir_ou/seismograms
mkdir $dir_ou/snapshots
mkdir $dir_ou/vtk_files

# move job_info files
mv job_info $dir_ou/

# copy input files
cp -p acqui $dir_ou/
cp -p acqui_id $dir_ou/
cp -p fsource $dir_ou/
cp -p extra_mesh_info.in $dir_ou/
cp -p *_input_namelist $dir_ou/
cp -p *_cart $dir_ou/model_cart/
cp -p topo $dir_ou/
cp -p weight_data $dir_ou/

# move output files
mv acqui_topo $dir_ou/
mv mesh $dir_ou/
mv extra_mesh_info $dir_ou/
mv *_gll  $dir_ou/model_gll/
mv gradient $dir_ou/model_gll/
mv fsismos* $dir_ou/seismograms/
mv *field $dir_ou/snapshots/
mv *.vtk $dir_ou/vtk_files/

# clean empty dirs
rmdir $dir_ou/model_cart  2>/dev/null
rmdir $dir_ou/model_gll   2>/dev/null
rmdir $dir_ou/seismograms 2>/dev/null
rmdir $dir_ou/snapshots   2>/dev/null
rmdir $dir_ou/vtk_files   2>/dev/null

