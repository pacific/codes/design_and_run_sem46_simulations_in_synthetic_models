------------------------------------------------------------------------

## Design and run SEM46 simulations in synthetic models

This package of codes is maintained via Git and hosted at  

`https://gricad-gitlab.univ-grenoble-alpes.fr/pacific/design_and_run_sem46_simulations_in_synthetic_models`  

It is regularly updated and you should make sure that you are using the
latest version.

Francois Lavoue (francois.lavoue@univ-grenoble-alpes.fr), 21 May 2019  
Last updated FL, 24 July 2020

------------------------------------------------------------------------

#### Outline of this readme:

- Workflow
- Contents
- Notes
- Requirements
- To do

------------------------------------------------------------------------

#### Workflow: follow the numbering of the shell scripts in order to

1. design synthetic models and inputs for SEM46 simulations (`0*.sh`),

2. run SEM46 simulations (`1*.sh`),

3. post-process and plot the results (`2*.sh`, `3*.[sh/ipynb]`).

These scripts should have self-explanatory names and contain detailed
comments. Some of them are wrappers for other scripts located in ./scripts
(see the latter for details then, in particular the preamble in `scripts/
build_simple_synthetic_model_*D.py`).

------------------------------------------------------------------------

#### Contents:

- `figs` : where to put general figures. Currently contains examples of
           model templates in order to give an idea of the capabilities
           of the program for designing simple synthetic models.  
             See also `results/output_run_*/figs` for figures obtained
           after processing SEM46 outputs for specific simulations.  
   Note that all these figures should be reproducible from the templates.

- `input_files` : contains input files for building synthetic models
                  (input_files/build_simple_synthetic_models.ins)
                and for running SEM46 simulations
                  (input_files/SEM46_input_files)
                including the templates used by 04_make_SEM46_input_files.sh
                  (input_files/SEM46_input_files/templates)
                and by 10_link_SEM46_inputs.sh
                  (input_files/SEM46_input_files/run_*)

- `job_info` : where we gather logs of standard outputs and errors.

- `results` : where we gather simulation results for post-processing.

- `scripts` : scripts used as subroutines by the main shell scripts of
              the workflow, including the python scripts for building
              simple synthetic models.

------------------------------------------------------------------------

#### Requirements:

- The SEM46 software itself is developed by the Seiscope consortium and
  is not publicly available. This package does not contain any of it.
  You should have your own version of SEM46 to run the corresponding
  steps of the workflow (`1*.sh`). Contact: Romain Brossier (ISTerre,
  romain.brossier@univ-grenoble-alpes.fr).

- This package also makes use of the Seiscope PyTools, which should be
  in your PYTHONPATH. Contact: Theodosius Marwan Irnaka (ISTerre,
  theodosius-marwan.irnaka@univ-grenoble-alpes.fr).

- Required Python environment:  
  (version numbers below work but may not be mandatory)
  ```
     $ conda env list   # (selected output)
     python               3.7
     basemap              1.2.0    (not used yet but maybe in the future)
     basemap-data-hires   1.2.0    ( "   "    "   "    "    "  "    "   )
     cartopy              0.17.0   ( "   "    "   "    "    "  "    "   )
     h5py                 2.9.0    ( "   "    "   "    "    "  "    "   , e.g. for pycorr)
     ipdb                 0.12     (for seiscope_pytools)
     jupyter              1.0.0    (for notebooks)
     matplotlib           3.0.2
     mayavi               4.7.0    (not used yet but maybe in the future)
     notebook             5.7.4
     numpy                1.15.4
     obspy                1.1.0
     pandas               0.24.1   (not used yet but maybe in the future)
     pyevtk               1.1.2    (for seiscope_pytools)
     pyproj               1.9.6    (not used yet but maybe in the future)
     scipy                1.2.1
  ```

- In addition, the post-processing notebook `30_*.ipynb` uses the
  following packages for computing group and phase velocity curves:
  * Laurent Stehly's `measure_disp2` function from its TOMO repo:  
      `git@gricad-gitlab.univ-grenoble-alpes.fr:stehlyl/tomo.git`
  * the `vg_fta` executable contained in the MSNoise package:  
      `https://github.com/ROBelgium/MSNoise.git`
  * Emmanuel Kaestle's `amb_noise_tools` for computing phase velocities:  
      `https://github.com/ekaestle/amb_noise_tools.git`  
    (to do...)

  You may use the package without these dependencies, but you need them
  to run the corresponding cells in the notebook.

- NB: my (FL) favorite way to manage Python environment is via Anaconda
  (https://www.anaconda.com):
  ```
     conda create -n env_python3.7 python=3.7 basemap basemap-data-hires cartopy h5py ipdb jupyter matplotlib mayavi notebook numpy obspy pandas pyevtk pyproj scipy
  ```
  (Note that it is recommended to install all required packages when
  creating the environment, as adding them later may lead to conflicts.)

------------------------------------------------------------------------

#### Notes:

- The models were initially based on simple geometries suggested by Dan
  Hollis and Richard Lynch (Sisprobe), but the package has evolved such
  that it can now be used as a generic workflow for designing, running
  and post-processing SEM46 simulations.

- All these programs assume a downward-pointing z-axis (same convention
  as SEM46).

- About the 2D vs. 3D versions of `build_simple_synthetic_model_*D.py` :
  - The 2D version is useful to quickly vizualize simple geometries.
  - You may then extend the resulting 2D models into 3D by assuming
    lateral invariance (see [02_]extend_2D_model_to_3D.[sh/py]),
    or turn to the 3D version for generating fully-3D models.
  - Note that the outputs from the 2D version may contain NaNs in case
    of topography. These NaNs are replaced by vertically-invariant
    extrapolation of the surface values in the 3D version and in
    `extend_2D_model_to_3D.py`.

- About depth, dip and azimuth of interfaces in `build_simple_synthetic_model_*D.in`:
  - For a dipping interface, the "depth" of the interface corresponds to
    the intersect with the z-axis (x = xmin). It is up to you to define
    this "depth" such that the interface intersects the surface (x-axis,
    z = 0) at the desired location, noting that
      "depth" = (x_intersect - xmin) * sin(dip)
  - The azimuth of dipping interfaces in 3D is not implemented yet,
    i.e. we cannot generate dipping interfaces with different azimuths,
    (i.e. interface planes are all parallel to each other, with a dip
    direction parallel to the x-axis).
      But we can rotate the acquisition geometry by a given azimuth,
    such that it does not align with the dip direction of the layers
    (see 14_make_acqui_file.sh).

------------------------------------------------------------------------

#### To do: (master)

- Compute phase velocity curves in `30*.ipynb` (e.g. using Emanuel Kaestle's
  Ambient Noise Tools: `https://github.com/ekaestle/amb_noise_tools`).

- Make the package compatible with Specfem I/Os.

- Re-program the core of `build_simple_model_3D.py` in C or Fortran for
  faster computation.

- Re-organize as functions and modules that could be used in notebooks
  for direct visualisation.

- Extend to real case studies (with scripts to process topography, etc)
  (update: this now done in application branches)

------------------------------------------------------------------------

