#!/usr/bin/env python
# ----------------------------------------------------------------------
#
# Build a synthetic model with simple geometries, as suggested by Dan
# and Richard (3D version).
#
# Francois Lavoue, 21 May 2019
# Last updated FL, 24 Sep. 2019
#
# ----------------------------------------------------------------------
#
# Notes:
#
# - Templates for input files can be found in
#     input_files/build_simple_synthetic_models.ins/
#
# - The base model consists of N horizontal layers, built from top to
#   bottom (i.e. each successive layer overlays the previous one: this
#   is useful to know in order to trick the program and generate more
#   complex structures, see the templates build_simple_synthetic_model_
#   2D_v0*.in for examples).
#
# - You can then specify options to add slopes, gradients, anomalies,
#   Gaussian-shaped "bumps" of given width and height, and sinusoidal
#   "wavy" topography of given wavelengths and amplitudes.
#
# - The structure of the input file is the following:
#     path/to/output/folder   # where the generated model will be stored in binary format (NB: do NOT add any comment to this line)
#     0.0  10.0  0.1  # zmin, zmax, dz (in m)
#     0.0  20.0  0.1  # xmin, xmax, dx    "
#     0.0  20.0  0.1  # ymin, ymax, dy    "
#     4               # nb of interfaces (including the surface, i.e. nb of layers)
#     0.0 5500 4000 3000 1000 100 0.  10.   # depth (m), Vp (m/s), Vs (m/s), rho (kg/m3), Qp, Qs, dip (deg), azimuth (deg)
#     7.0 6000 4500 3200 1000 100 30.  0.   # (as many lines like the above as interfaces...)
#     4.0 6700 4700 3500 1000 100 0.  45.
#     8.0 7000 5000 3700 1000 100 0.  60.
#     3               # nb of layers with gradients
#     0 1  100   50   20   0.   0.    # index of layer with gradient, gradient wrt absolute depth (0) or depth below interface (1), \
#     1 0  100   50   20   0.   0.    #                     grad(Vp) ([m/s]/m), grad(Vs) ([m/s]/m), grad(rho) ([kg/m3]/m), grad(Qp), grad(Qs)
#     2 0  100   50   20   0.   0.    # (as many lines like the above as layers with gradients)
#     2                   # nb of "bumpy" interfaces (to which we will add Gaussian-shaped bumps)
#     0  4.  8. 12.  4.   #    index of bumpy interface, bump location [x0,y0] (m), Gaussian width [dx,dy] (6*sigma, m), Gaussian height dz (m)
#     2  6. 14.  4. -2.   #    (as many lines like the above as bumpy interfaces)
#     2                   # nb of "wavy" interfaces (NB: an interface can be both "bumpy" and "wavy")
#     0  2. 0.25          #    index of wavy interface, wavelength (m), amplitude (m)
#     3  5. 0.5           #    (as many lines like the above as wavy interfaces)
#     1                   # nb of (rectangular) anomalies
#     6. 5.  5. 2.  12. 4.            # location and size of the anomaly (beware of the order: [z0,dz,x0,dx,y0,dy])
#     6500. 4500. 3500. 1000. 100.    # properties of the anomaly: Vp (m/s), Vs (m/s), rho (kg/m3), Qp, Qs
#     z0_ano dz_ano x0_ano dx_ano [y0_ano dy_ano]   #   (as many lines like the above as rectangular anomalies)
#     vp_ano vs_ano rho_ano qp_ano qs_ano           #   ...
#
# - Note that we assume a downward-pointing z-axis.
#
# - If plot_model=True below, the program will create a file `figs/simple_
#   synthetic_model_3D.pdf` for quick visualisation of the generated model.
#   Figures corresponding to the input templates in `input_files/build_
#   simple_synthetic_models.ins` can be found in `figs/simple_synthetic_models`.
#
# ----------------------------------------------------------------------

#-- uncomment this for Python2 forward compatibility
from __future__ import print_function

#-- import modules
from matplotlib import gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.pyplot as plt
import numpy as np
import shutil
import struct
import os


#-- user parameters
file_in = "build_simple_synthetic_model_3D.in"

# plot options
plot_model = True
file_ou_fig = './figs/simple_synthetic_model_3D'
cmap = plt.cm.jet   #seismic_r
wfig = 8
hfig = 8

# debug options
debug = False

if debug:
    fid_debug = open("debug.out",'w')


#-- read input file
fid = open(file_in,'r')

# read output directory
line = fid.readline().strip().split()
dir_ou = str(line[0])

# read domain boundaries
# (beware of the order, [z,x,y], both for 2D/3D management and for compatibility with SEM array format)
line = fid.readline().strip().split()
zmin = np.float(line[0])
zmax = np.float(line[1])
dz   = np.float(line[2])
line = fid.readline().strip().split()
xmin = np.float(line[0])
xmax = np.float(line[1])
dx   = np.float(line[2])
line = fid.readline().strip().split()
ymin = np.float(line[0])
ymax = np.float(line[1])
dy   = np.float(line[2])
print("zmin, zmax, dz =", zmin, zmax, dz)
print("xmin, xmax, dx =", xmin, xmax, dx)
print("ymin, ymax, dy =", ymin, ymax, dy)


# read nb of interfaces
line = fid.readline().strip().split()
n_int = np.int_(line[0])
print("nb of interfaces =",n_int)
#file_ou_fig = file_ou_fig + '_' + str(n_int) + '-interfaces'

# init. 1D arrays
z_in  = np.nan*np.zeros(n_int)
vp_in = np.nan*np.zeros(n_int)
vs_in = np.nan*np.zeros(n_int)
rho_in= np.nan*np.zeros(n_int)
qp_in = np.nan*np.zeros(n_int)
qs_in = np.nan*np.zeros(n_int)
sl_in = np.nan*np.zeros(n_int)
az_in = np.nan*np.zeros(n_int)

# read interfaces/layers properties
for i_int in range(0,n_int):
    line = fid.readline().strip().split()
    z_in[i_int]  = np.float(line[0])   # depth of interface
    vp_in[i_int] = np.float(line[1])   # Vp in layer below interface (in m/s)
    vs_in[i_int] = np.float(line[2])   # Vs  "   "     "       "      "   "
    rho_in[i_int]= np.float(line[3])   # density "     "       "     (in kg/m3)
    qp_in[i_int] = np.float(line[4])   # Qp  "   "     "       "
    qs_in[i_int] = np.float(line[5])   # Qs  "   "     "       "
    sl_in[i_int] = np.float(line[6])   # slope/dip of interface (in degrees)
    az_in[i_int] = np.float(line[7])   # azimuth of interface (in degrees)

    # check for vertical slopes (not enabled yet)
    if abs(abs(sl_in[i_int])-90.) < 1.e-6:
        print("Error: interface nb %i is vertical : dip = %g degree." % (i_int,sl_in[i_int]))
        exit("Error: vertical slopes not allowed yet.")

    # check for non-zero azimuths (not implemented yet)
    if abs( az_in[i_int] ) > 0.0 :
        print("Error: interface nb %i has a non-zero azimuth : az = %g degree." % (i_int,az_in[i_int]))
        print("       Non-zero azimuths are not implemented yet.")
        exit("Error: non-zero azimuths not implemented yet.")


# read nb of layers with gradients
line = fid.readline().strip().split()
n_grad = np.int_(line[0])
print("nb of layers with gradients =",n_grad)

# read properties of layers with gradients
flag_grad = np.zeros(n_int)    # have the layers gradients? (yes/no)
flag_grad_topo = np.zeros(n_int)    # gradient according to absolute depth (0) or depth below interface (1)
vp_grad = np.zeros(n_int)      # Vp gradients for each layer
vs_grad = np.zeros(n_int)      # Vs gradients
rh_grad = np.zeros(n_int)      # density gradients
qp_grad = np.zeros(n_int)      # Qp gradients
qs_grad = np.zeros(n_int)      # Qs gradients

for i_grad in range(0,n_grad):
    line = fid.readline().strip().split()     # read new line in input file
    i_int_grad = np.int_(line[0])             # index of this layer with gradients
    flag_grad[i_int_grad] = 1                 # this interface has a gradient
    flag_grad_topo[i_int_grad] = np.float(line[1])   # gradient according to absolute depth (0) or depth below interface (1)
    vp_grad[i_int_grad] = np.float(line[2])   # Vp gradient
    vs_grad[i_int_grad] = np.float(line[3])   # Vs gradient
    rh_grad[i_int_grad] = np.float(line[4])   # density gradient
    qp_grad[i_int_grad] = np.float(line[5])   # Qp gradient
    qs_grad[i_int_grad] = np.float(line[6])   # Qs gradient


# read nb of "bumpy" interfaces
line = fid.readline().strip().split()
n_bumpy = np.int_(line[0])
print("nb of bumpy interfaces =",n_bumpy)

# read properties of "bumpy" interfaces
flag_bumpy = np.zeros(n_int)   # are the interfaces "bumpy"? (yes/no)
x0_bumps = np.zeros(n_int)     # bump x-locations
y0_bumps = np.zeros(n_int)     # bump y-locations
sigx_bumps = np.ones(n_int)    # x-widths (stds) of the Gaussian bumps
sigy_bumps = np.ones(n_int)    # y-widths (stds) of the Gaussian bumps
amp_bumps = np.zeros(n_int)    # bump heights
amp_bump_topo = 0.             # topographical bump (for correcting z_min later on)
for i_bump in range(0,n_bumpy):
    line = fid.readline().strip().split()       # read new line in input file
    i_int_bumpy = np.int_(line[0])              # index of "bumpy" interface
    flag_bumpy[i_int_bumpy] = 1                 # this interface is "bumpy"
    x0_bumps[i_int_bumpy] = np.float(line[1])   # bump x-location
    y0_bumps[i_int_bumpy] = np.float(line[2])   # bump y-location
    sigx6_bump_tmp = np.float(line[3])          # bump x-width (given as 6*sigma of the Gaussian)
    sigy6_bump_tmp = np.float(line[4])          # bump y-width (given as 6*sigma of the Gaussian)
    amp_bump_tmp = np.float(line[5])            # bump height
    if i_int_bumpy==0 and amp_bump_tmp>0:       # remember (positive) topographical bump for correcting z_min later on
        amp_bump_topo = amp_bump_tmp

    # convert bumps widths to stds (input widths are given as 6*sigma)
    sigx_bumps[i_int_bumpy] = sigx6_bump_tmp / 6.
    sigy_bumps[i_int_bumpy] = sigy6_bump_tmp / 6.

    # compute Gaussian amplitude coefficients in order to match the true heights given in input
    x_tmp = np.arange(-sigx_bumps[i_int_bumpy],sigx_bumps[i_int_bumpy],sigx_bumps[i_int_bumpy]/100)
    y_tmp = np.arange(-sigx_bumps[i_int_bumpy],sigx_bumps[i_int_bumpy],sigx_bumps[i_int_bumpy]/100)
    gmax = 0.0
    for xi in x_tmp :
        for yi in y_tmp :
            g_tmp = np.exp( -xi**2 / (2*sigx_bumps[i_int_bumpy]**2) - yi**2 / (2*sigy_bumps[i_int_bumpy]**2) )
                    #/ np.sqrt(2*np.pi*sigx_bumps[i_int_bumpy]**2)
            gmax = max(gmax,g_tmp)
    amp_bumps[i_int_bumpy] = amp_bump_tmp / gmax


# read nb of "wavy" interfaces
line = fid.readline().strip().split()
n_wavy = np.int_(line[0])
print("nb of wavy interfaces =",n_wavy)

# read properties of "wavy" interfaces
flag_wavy = np.zeros(n_int)    # are the interfaces "wavy"? (yes/no)
lambda_wavs = np.ones(n_int)   # wavelengths of the undulations
amp_wavs = np.zeros(n_int)     # amplitudes of the undulations
for i_wavy in range(0,n_wavy):
    line = fid.readline().strip().split()         # read new line in input file
    i_int_wavy = np.int_(line[0])                 # index of "wavy" interface
    flag_wavy[i_int_wavy] = 1                     # this interface is "wavy"
    lambda_wavs[i_int_wavy] = np.float(line[1])   # wavelength of the undulations
    amp_wavs[i_int_wavy] = np.float(line[2])      # amplitude of the undulations


# read nb of rectangular anomalies
line = fid.readline().strip().split()
n_ano = np.int_(line[0])
print("nb of anomalies =",n_ano)

# init. properties of anomalies
z0_ano = np.nan*np.zeros(n_ano)
dz_ano = np.nan*np.zeros(n_ano)
x0_ano = np.nan*np.zeros(n_ano)
dx_ano = np.nan*np.zeros(n_ano)
y0_ano = np.nan*np.zeros(n_ano)
dy_ano = np.nan*np.zeros(n_ano)
vp_ano = np.nan*np.zeros(n_ano)
vs_ano = np.nan*np.zeros(n_ano)
rho_ano= np.nan*np.zeros(n_ano)
qp_ano = np.nan*np.zeros(n_ano)
qs_ano = np.nan*np.zeros(n_ano)

for i_ano in range(0,n_ano):
    # read geometry of anomalies
    line = fid.readline().strip().split()   # read new line in input file
    z0_ano[i_ano] = np.float(line[0])       # z-location of anomaly (centre)
    dz_ano[i_ano] = np.float(line[1])       # z-width    "     "
    x0_ano[i_ano] = np.float(line[2])       # x-location "     "       "
    dx_ano[i_ano] = np.float(line[3])       # x-width    "     "
    y0_ano[i_ano] = np.float(line[4])       # y-location "     "       "
    dy_ano[i_ano] = np.float(line[5])       # y-width    "     "

    # read properties of anomalies
    line = fid.readline().strip().split()   # read new line in input file
    vp_ano[i_ano] = np.float(line[0])       # vp of anomaly (m/s)
    vs_ano[i_ano] = np.float(line[1])       # vs  "    "    (m/s)
    rho_ano[i_ano]= np.float(line[2])       # rho "    "    (kg/m3)
    qp_ano[i_ano] = np.float(line[3])       # qp  "    "
    qs_ano[i_ano] = np.float(line[4])       # qs  "    "

#-- end read input file


#-- adapt z_min in case of topography (i.e. non-flat 1st interface)
zmin = zmin - amp_wavs[0] - amp_bump_topo

#-- build (default) coordinates vectors
vz = np.arange(zmin,zmax+0.1*dz,dz)
nz = len(vz)
vx = np.arange(xmin,xmax+0.1*dx,dx)
nx = len(vx)
vy = np.arange(ymin,ymax+0.1*dy,dy)
ny = len(vy)
# NB: "xmax+0.1*dx" is a trick to actually get values from xmin to xmax, included.

#-- init. 3D arrays
# (beware of the order, [nz,nx,ny], both for 2D/3D management and for compatibility with SEM array format)
vp = np.nan*np.zeros((nz,nx,ny))
vs = np.nan*np.zeros((nz,nx,ny))
rh = np.nan*np.zeros((nz,nx,ny))
qp = np.nan*np.zeros((nz,nx,ny))
qs = np.nan*np.zeros((nz,nx,ny))
ztopo = np.nan*np.zeros((nx,ny))


#-- Build model
for iz in range(0,len(vz)):
    zi = vz[iz]

    for ix in range(0,len(vx)):
        xi = vx[ix]

        for iy in range(0,len(vy)):
            yi = vy[iy]

            #-- loop over interfaces/layers
            for i_int in range(0,n_int):

                #-- compute depth of interface, taking dip, bumps and waves into account
                z_int = z_in[i_int] \
                      - xi * np.tan(np.pi*sl_in[i_int]/180.) \
                      + amp_wavs[i_int]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int] ) \
                      - amp_bumps[i_int] * np.exp( -np.square(xi-x0_bumps[i_int]) / (2*sigx_bumps[i_int]**2) \
                                                   -np.square(yi-y0_bumps[i_int]) / (2*sigy_bumps[i_int]**2) )
                                         #/ np.sqrt(2*np.pi*sigx_bumps[i_int]**2)

                if i_int == 0 and iz == 0 :
                   # the 1st interface gives the topography of the model
                   ztopo[ix,iy] = z_int

                #-- also (re)compute the depth of all other interfaces in order to avoid intersections
                # interfaces above current one
                z_int_above = -np.inf   # init.
                for i_int2 in range(0,i_int):
                    z_int_above_tmp = z_in[i_int2] \
                                    - xi * np.tan(np.pi*sl_in[i_int2]/180.) \
                                    + amp_wavs[i_int2]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int2] ) \
                                    - amp_bumps[i_int2] * np.exp( -np.square(xi-x0_bumps[i_int2]) / (2*sigx_bumps[i_int2]**2)
                                                                  -np.square(yi-y0_bumps[i_int2]) / (2*sigy_bumps[i_int2]**2) )
                                                        #/ np.sqrt(2*np.pi*sigx_bumps[i_int2]**2)
                    z_int_above = max(z_int_above,z_int_above_tmp)

                # interfaces below current one
                z_int_below = np.inf   # init.
                for i_int2 in range(i_int+1,n_int-(i_int+1)):
                    z_int_below_tmp = z_in[i_int2] \
                                    - xi * np.tan(np.pi*sl_in[i_int+1]/180.) \
                                    + amp_wavs[i_int2]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int2] ) \
                                    - amp_bumps[i_int2] * np.exp( -np.square(xi-x0_bumps[i_int2]) / (2*sigx_bumps[i_int2]**2)
                                                                  -np.square(yi-y0_bumps[i_int2]) / (2*sigy_bumps[i_int2]**2) )
                                                        #/ np.sqrt(2*np.pi*sigx_bumps[i_int2]**2)
                    z_int_below = min(z_int_below,z_int_below_tmp)


                #-- (manage dipping interfaces)
                if ( abs(sl_in[i_int])<1.e-3 and zi >= z_int) or \
                   ( abs(sl_in[i_int])>1.e-3 and zi >= z_int and zi >= z_int_above and zi <= z_int_below ):

                   if flag_grad_topo[i_int] == 0 :
                      # apply gradient wrt absolute depth
                      if abs(sl_in[i_int]) > 0.0 :
                         if ix == 0 and iz == 0 :
                            print("Warning: this is a dipping layer and you want to apply a gradient")
                            print("according to absolute depth: be aware that it is the input depth")
                            print("of the previous layer that is considered in this case.\n")
                         if i_int == 0 :
                            print("Error: no previous layer for defining absolute depth.")
                            exit("Error: no previous layer for defining absolute depth.")
                         z_grad = zi-z_in[i_int-1]
                      else :
                         z_grad = zi-z_in[i_int]
                   elif flag_grad_topo[i_int] == 1 :
                      # apply gradient wrt depth below interface
                      z_grad = zi-z_int

                   #-- assign layer properties
                   vp[iz,ix,iy] = vp_in[i_int] + vp_grad[i_int]*z_grad
                   vs[iz,ix,iy] = vs_in[i_int] + vs_grad[i_int]*z_grad
                   rh[iz,ix,iy] = rho_in[i_int] + rh_grad[i_int]*z_grad
                   qp[iz,ix,iy] = qp_in[i_int] + qp_grad[i_int]*z_grad
                   qs[iz,ix,iy] = qs_in[i_int] + qs_grad[i_int]*z_grad
                   del z_grad
                #-- end if dipping interface


                #-- debug checks
                if debug:
                    if ( abs(sl_in[i_int])<1.e-3 and zi >= z_int):
                        fid_debug.write("interface nb %i is not dipping: simply assign value to [ix,iz] = [%i,%i] at (xi,zi) = (%g,%g)\n" \
                                        % (i_int,ix,iz,xi,zi) )
                    elif ( abs(sl_in[i_int])>1.e-3 and zi >= z_int and zi >= z_int_above and zi <= z_int_below ):
                        fid_debug.write("interface nb %i is     dipping:        assign value to [ix,iz] = [%i,%i] at (xi, zi) = (%g,%g)\n" \
                                        % (i_int,ix,iz,xi,zi) )
                        fid_debug.write("          NB: depths of interfaces above/below = %g / %g\n" % (z_int_above,z_int_below) )

            #-- end loop over interfaces (for i_int)

            #-- loop over anomalies
            for i_ano in range(0,n_ano):
                #-- assign anomaly properties (overwrite layer properties)
                if xi>=x0_ano[i_ano]-0.5*dx_ano[i_ano] and xi<=x0_ano[i_ano]+0.5*dx_ano[i_ano] and \
                   yi>=y0_ano[i_ano]-0.5*dy_ano[i_ano] and yi<=y0_ano[i_ano]+0.5*dy_ano[i_ano] and \
                   zi>=z0_ano[i_ano]-0.5*dz_ano[i_ano] and zi<=z0_ano[i_ano]+0.5*dz_ano[i_ano]:
                    vp[iz,ix,iy] = vp_ano[i_ano]
                    vs[iz,ix,iy] = vs_ano[i_ano]
                    rh[iz,ix,iy] = rho_ano[i_ano]
                    qp[iz,ix,iy] = qp_ano[i_ano]
                    qs[iz,ix,iy] = qs_ano[i_ano]
            #-- end for i_ano

        #-- end for iy
    #-- end for ix
#-- end for iz


#-- plot model (before printing to file in order to plot NaNs in white)
if plot_model:
    # init. fig
    fig = plt.figure(figsize=(wfig,hfig))
    gs = gridspec.GridSpec(2, 2, width_ratios=[1,1], height_ratios=[1,1])
    # FL: ratios could be optimized...

    ax_xy = plt.subplot(gs[1,0])
    ax_xz = plt.subplot(gs[0,0], sharex=ax_xy)
    ax_yz = plt.subplot(gs[1,1], sharey=ax_xy)
    # FL: "sharey" does not work optimally, y-axis don't have the same size...

    # FL: this below does not work better...
    #fig, ax = plt.subplots(ncols=2, nrows=2, sharex='col', sharey='row', figsize=(wfig,hfig))
    ##                       gridspec_kw={'hspace': 0, 'wspace': 0})
    #ax_xy = ax[1,0]
    #ax_xz = ax[0,0]
    #ax_yz = ax[1,1]
    #ax[0,1].axis("off")

    if n_ano > 0 :
       # plot sections through 1st anomaly
       ix0 = np.int_(np.floor(x0_ano[0]/dx))
       iy0 = np.int_(np.floor(y0_ano[0]/dy))
       iz0 = np.int_(np.floor(z0_ano[0]/dz))
    else :
       # plot sections in the middle of the model
       ix0 = np.int_(np.floor(0.5*(xmin+xmax)/dx))
       iy0 = np.int_(np.floor(0.5*(ymin+ymax)/dy))
       iz0 = np.int_(np.floor(0.5*(zmin+zmax)/dz))

    # find min/max
    vmin = min( np.nanmin(vp[iz0,:,:]), np.nanmin(vp[:,:,iy0]), np.nanmin(vp[:,ix0,:]) )
    vmax = max( np.nanmax(vp[iz0,:,:]), np.nanmax(vp[:,:,iy0]), np.nanmax(vp[:,ix0,:]) )

    # plot xy-slice at given z
    zplot = ax_xy.imshow(vp[iz0,:,:], extent=[xmin,xmax,ymin,ymax], origin='lower', cmap=cmap, vmin=vmin, vmax=vmax)
    ax_xy.text(xmin+0.05*(xmax-xmin), ymin+0.90*(ymax-ymin), ("z = %g m"%(iz0*dz)), ha="left", va="top", color="white", fontweight="bold")

    # plot xz-slice at given y
    xplot = ax_xz.imshow(vp[:,:,iy0], extent=[xmin,xmax,zmin,zmax], origin='lower', cmap=cmap, vmin=vmin, vmax=vmax)
    ax_xz.text(xmin+0.05*(xmax-xmin), zmin+0.10*(zmax-zmin), ("y = %g m"%(iy0*dy)), ha="left", va="top", color="white", fontweight="bold")

    # plot yz-slice at given x
    yplot = ax_yz.imshow(vp[:,ix0,:].transpose(), extent=[zmin,zmax,ymin,ymax], origin='lower', cmap=cmap, vmin=vmin, vmax=vmax)
    ax_yz.text(zmin+0.05*(zmax-zmin), ymin+0.05*(ymax-ymin), ("x = %g m"%(ix0*dx)), ha="left", va="bottom", color="white", fontweight="bold", rotation=90)

    # axis (xz-slice)
    ax_xz.set_xlabel("x (m)")
    ax_xz.set_ylabel("z (m)")
    ax_xz.set_xlim(xmin,xmax)
    ax_xz.set_ylim(zmin,zmax)
    ax_xz.xaxis.tick_top()
    ax_xz.xaxis.set_label_position('top')
    ax_xz.invert_yaxis()

    # axis (xy-slice)
    ax_xy.set_xlabel("x (m)")
    ax_xy.set_ylabel("y (m)")
    ax_xy.set_xlim(xmin,xmax)
    ax_xy.set_ylim(ymin,ymax)

    # axis (y-slice)
    ax_yz.set_xlabel("z (m)")
    ax_yz.set_ylabel("y (m)")
    ax_yz.set_xlim(zmin,zmax)
    ax_yz.set_ylim(ymin,ymax)
    ax_yz.yaxis.tick_right()
    ax_yz.yaxis.set_label_position('right')

    ## colorbar
    #divider = make_axes_locatable(ax_yz)
    #cax = divider.append_axes("right", size="5%", pad=0.25)
    #cbar = plt.colorbar(yplot,cax=cax,orientation='vertical')
    #cbar.set_label("$V_P$ (m/s)")

    cb_axes = inset_axes(ax_xy,
              width="60%",   # width = % of parent_bbox width
              height="15%",  # height
              loc="lower left",
              bbox_to_anchor=(1.4, 0.5, 1.0, 1.0),
              bbox_transform=ax_xz.transAxes)
    #         borderpad=0)
    cbar = fig.colorbar(zplot, cax=cb_axes, orientation='horizontal')
    cbar.set_label("$V_P$ (m/s)")

    # save figure
    fig.savefig(file_ou_fig + ".pdf", format="pdf")

#-- end if plot_model


#-- replace NaNs by vertical extrapolation of surface values
i_nans_z0 = np.where( np.isnan(vp[0,:,:]) )
if len(i_nans_z0[0]) > 0 :
   print("\nloop over %i NaNs found at z = 0." % len(i_nans_z0[0]))
for (ix_nan,iy_nan) in zip(i_nans_z0[0],i_nans_z0[1]) :
   i_nans_loc = np.where( np.isnan(vp[:,ix_nan,iy_nan]) )
   #print("ix_nan = %i,   iy_nan = %i,   NaN replaced by %g" % (ix_nan,iy_nan,vp[i_nans_loc[0][-1]+1,ix_nan,iy_nan]))
   vp[i_nans_loc,ix_nan,iy_nan] = vp[i_nans_loc[0][-1]+1,ix_nan,iy_nan]
   vs[i_nans_loc,ix_nan,iy_nan] = vs[i_nans_loc[0][-1]+1,ix_nan,iy_nan]
   rh[i_nans_loc,ix_nan,iy_nan] = rh[i_nans_loc[0][-1]+1,ix_nan,iy_nan]
   qp[i_nans_loc,ix_nan,iy_nan] = qp[i_nans_loc[0][-1]+1,ix_nan,iy_nan]
   qs[i_nans_loc,ix_nan,iy_nan] = qs[i_nans_loc[0][-1]+1,ix_nan,iy_nan]

#-- redatum topo (SEM46 does not like negative topo values)
print("min(ztopo) = %g m" % ztopo.min())
if ztopo.min() < 0.0 :
   #z_datum = 10**np.ceil(np.log10(-ztopo.min()))   # round to next order of magnitude
   #z_datum = 100*np.ceil(-ztopo.min()/100)         # round to next multiple of 100 m
   oom = 10**np.ceil(np.log10(-ztopo.min())-1)      # find order of magnitude (oom)
   z_datum = oom*np.ceil(-ztopo.min()/oom)          # round to this order of magnitude
else :
   z_datum = 0.0
print("z_datum = %g m" % z_datum)
ztopo = ztopo + z_datum


#-- make output directory
if not os.path.isdir(dir_ou): os.makedirs(dir_ou)

#-- print model to file
#if output_format == "bin_sem46" :
file_ou = (dir_ou + "/vp_n%ix%ix%i_h%gx%gx%gm" % (nz,nx,ny,dz,dx,dy))
fid = open(file_ou + ".bin","wb")
for iy in range(0,ny) :
    fid.write(struct.pack('f'*len(vp.flatten('F')), *vp.flatten('F')))
fid.close()

file_ou = (dir_ou + "/vs_n%ix%ix%i_h%gx%gx%gm" % (nz,nx,ny,dz,dx,dy))
fid = open(file_ou + ".bin","wb")
for iy in range(0,ny) :
    fid.write(struct.pack('f'*len(vs.flatten('F')), *vs.flatten('F')))
fid.close()

file_ou = (dir_ou + "/rho_n%ix%ix%i_h%gx%gx%gm" % (nz,nx,ny,dz,dx,dy))
fid = open(file_ou + ".bin","wb")
for iy in range(0,ny) :
    fid.write(struct.pack('f'*len(rh.flatten('F')), *rh.flatten('F')))
fid.close()

file_ou = (dir_ou + "/qp_n%ix%ix%i_h%gx%gx%gm" % (nz,nx,ny,dz,dx,dy))
fid = open(file_ou + ".bin","wb")
for iy in range(0,ny) :
    fid.write(struct.pack('f'*len(qp.flatten('F')), *qp.flatten('F')))
fid.close()

file_ou = (dir_ou + "/qs_n%ix%ix%i_h%gx%gx%gm" % (nz,nx,ny,dz,dx,dy))
fid = open(file_ou + ".bin","wb")
for iy in range(0,ny) :
    fid.write(struct.pack('f'*len(qs.flatten('F')), *qs.flatten('F')))
fid.close()

#-- print topo to file
file_ou = dir_ou + ("/ztopo_redatumed-%gm_n%ix%i_h%gx%gm" % (z_datum,nx,ny,dx,dy))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(ztopo.flatten('F')), *ztopo.flatten('F')))
fid.close()


#-- copy input file to output dir for further reference
shutil.copy(file_in, dir_ou)


#-- close debug file
if debug:
    fid_debug.close()

