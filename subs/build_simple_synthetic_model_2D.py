#!/usr/bin/env python
# ----------------------------------------------------------------------

# Build a synthetic model with simple geometries, as suggested by Dan
# and Richard (2D version).
#
# Francois Lavoue, 21 May 2019
# Last updated FL, 21 Sep. 2019

# ----------------------------------------------------------------------

# Notes:
#
# - Templates for input files can be found in
#     input_files/build_simple_synthetic_models.ins/

# - The base model consists of N horizontal layers, built from top to
#   bottom (i.e. each successive layer overlays the previous one: this
#   is useful to know in order to trick the program and generate more
#   complex structures, see the templates build_simple_synthetic_model_
#   2D_v0*.in for examples).

# - You can then specify options to add slopes, gradients, anomalies,
#   Gaussian-shaped "bumps" of given width and height, and sinusoidal
#   "wavy" topography of given wavelengths and amplitudes.

# - The structure of the input file is the following:
#     path/to/output/folder   # where the generated model will be stored in binary format (NB: do NOT add any comment to this line)
#     0.0  10.0  0.1  # zmin, zmax, dz (in m)
#     0.0  20.0  0.1  # xmin, xmax, dx    "
#     4               # nb of interfaces (including the surface, i.e. nb of layers)
#     0.0 5500 4000 3000 1000 100 0.   # depth (m), Vp (m/s), Vs (m/s), rho (kg/m3), Qp, Qs, dip (deg)
#     7.0 6000 4500 3200 1000 100 30.  # (as many lines like the above as interfaces...)
#     4.0 6700 4700 3500 1000 100 0.
#     8.0 7000 5000 3700 1000 100 0.
#     3               # nb of layers with gradients
#     0 1  100   50   20   0.   0.    # index of layer with gradient, gradient wrt absolute depth (0) or depth below interface (1), \
#     1 0  100   50   20   0.   0.    #                     grad(Vp) ([m/s]/m), grad(Vs) ([m/s]/m), grad(rho) ([kg/m3]/m), grad(Qp), grad(Qs)
#     2 0  100   50   20   0.   0.    # (as many lines like the above as layers with gradients)
#     2               # nb of "bumpy" interfaces (to which we will add Gaussian-shaped bumps)
#     0  8. 12.  4.   #    index of bumpy interface, bump location x0 (m), Gaussian width dx (6*sigma, m), Gaussian height dz (m)
#     2 14.  4. -2.   #    (as many lines like the above as bumpy interfaces)
#     2               # nb of "wavy" interfaces (NB: an interface can be both "bumpy" and "wavy")
#     0 2. 0.25       #    index of wavy interface, wavelength (m), wave amplitude (m)
#     3 5. 0.5        #    (as many lines like the above as wavy interfaces)
#     1               # nb of (rectangular) anomalies
#     6. 5.  5. 2.    # location and size of the anomaly : [z0,dz,x0,dx]
#     6500. 4500. 3500. 1000. 100.          # properties of the anomaly: Vp (m/s), Vs (m/s), rho (kg/m3), Qp, Qs
#     z0_ano dz_ano x0_ano dx_ano           #   (as many lines like the above as rectangular anomalies)
#     vp_ano vs_ano rho_ano qp_ano qs_ano   #   ...

# - Note that we assume a downward-pointing z-axis.

# - Topography is defined as the first vertical point where model (Vp)
#   values are not NaNs (/!\ cell point, not edge).

# - If plot_model=True below, the program will create a file `figs/simple_
#   synthetic_model_2D.pdf` for quick visualisation of the generated model.
#   Figures corresponding to the input templates in `input_files/build_
#   simple_synthetic_models.ins` can be found in `figs/simple_synthetic_models`.

# ----------------------------------------------------------------------


#-- uncomment this for Python2 forward compatibility
from __future__ import print_function

#-- import modules
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import os
import shutil
import struct


#-- user parameters
file_in = "build_simple_synthetic_model_2D.in"

# plot options
plot_model = True
file_ou_fig = './figs/simple_synthetic_model_2D'
cmap = plt.cm.jet
wfig = 8
hfig = 8

# debug options
debug = False

if debug:
    fid_debug = open("debug.out",'w')


#-- read input file
fid = open(file_in,'r')

# read output directory
line = fid.readline().strip().split()
dir_ou = str(line[0])

# read domain boundaries
# (beware of the order, [z,x,y], both for 2D/3D management and for compatibility with SEM array format)
line = fid.readline().strip().split()
zmin = np.float(line[0])
zmax = np.float(line[1])
dz   = np.float(line[2])
line = fid.readline().strip().split()
xmin = np.float(line[0])
xmax = np.float(line[1])
dx   = np.float(line[2])
print("xmin, xmax, dx =", xmin, xmax, dx)
print("zmin, zmax, dz =", zmin, zmax, dz)


# read nb of interfaces
line = fid.readline().strip().split()
n_int = np.int_(line[0])
print("nb of interfaces =",n_int)

# init. 1D arrays
z_in  = np.nan*np.zeros(n_int)
vp_in = np.nan*np.zeros(n_int)
vs_in = np.nan*np.zeros(n_int)
rho_in= np.nan*np.zeros(n_int)
qp_in = np.nan*np.zeros(n_int)
qs_in = np.nan*np.zeros(n_int)
sl_in = np.nan*np.zeros(n_int)

# read interfaces/layers properties
for i_int in range(0,n_int):
    line = fid.readline().strip().split()
    z_in[i_int]  = np.float(line[0])   # depth of interface
    vp_in[i_int] = np.float(line[1])   # Vp in layer below interface (in m/s)
    vs_in[i_int] = np.float(line[2])   # Vs  "   "     "       "      "   "
    rho_in[i_int]= np.float(line[3])   # density "     "       "     (in kg/m3)
    qp_in[i_int] = np.float(line[4])   # Qp  "   "     "       "
    qs_in[i_int] = np.float(line[5])   # Qs  "   "     "       "
    sl_in[i_int] = np.float(line[6])   # slope/dip of interface (in degrees)

    # check for vertical slopes (not enabled yet)
    if abs(abs(sl_in[i_int])-90.) < 1.e-6:
        print("Error: interface nb %i is vertical : dip = %g degree." % (i_int,sl_in[i_int]))
        exit("Error: vertical slopes not allowed yet.")


# read nb of layers with gradients
line = fid.readline().strip().split()
n_grad = np.int_(line[0])
print("nb of layers with gradients =",n_grad)

# read properties of layers with gradients
flag_grad = np.zeros(n_int)    # have the layers gradients? (yes/no)
flag_grad_topo = np.zeros(n_int)    # gradient according to absolute depth (0) or depth below interface (1)
vp_grad = np.zeros(n_int)      # Vp gradients for each layer (0 by default)
vs_grad = np.zeros(n_int)      # Vs gradients
rh_grad = np.zeros(n_int)      # density gradients
qp_grad = np.zeros(n_int)      # Qp gradients
qs_grad = np.zeros(n_int)      # Qs gradients

for i_grad in range(0,n_grad):
    line = fid.readline().strip().split()     # read new line in input file
    i_int_grad = np.int_(line[0])             # index of this layer with gradients
    flag_grad[i_int_grad] = 1                 # this interface has a gradient
    flag_grad_topo[i_int_grad] = np.float(line[1])   # gradient according to absolute depth (0) or depth below interface (1)
    vp_grad[i_int_grad] = np.float(line[2])   # Vp gradient
    vs_grad[i_int_grad] = np.float(line[3])   # Vs gradient
    rh_grad[i_int_grad] = np.float(line[4])   # density gradient
    qp_grad[i_int_grad] = np.float(line[5])   # Qp gradient
    qs_grad[i_int_grad] = np.float(line[6])   # Qs gradient


# read nb of "bumpy" interfaces
line = fid.readline().strip().split()
n_bumpy = np.int_(line[0])
print("nb of bumpy interfaces =",n_bumpy)

# read properties of "bumpy" interfaces
flag_bumpy = np.zeros(n_int)      # are the interfaces "bumpy"? (yes/no)
type_of_bumps = np.zeros(n_int)   # bumps types: 1->Gaussian, 2->triangular
x0_bumps = np.zeros(n_int)        # bumps x-locations
sigma_bumps = np.ones(n_int)      # bumps widths (i.e. stds for Gaussian bumps)
amp_bumps = np.zeros(n_int)       # bumps heights
amp_bump_topo = 0.                # topographical bump (for correcting z_min later on)
for i_bump in range(0,n_bumpy):
    line = fid.readline().strip().split()       # read new line in input file
    i_int_bumpy = np.int_(line[0])              # index of "bumpy" interface
    flag_bumpy[i_int_bumpy] = 1                 # this interface is "bumpy"
    type_of_bumps[i_int_bumpy] = np.float(line[1])   # bump type
    x0_bumps[i_int_bumpy] = np.float(line[2])        # bump x-location
    sigma6_bump_tmp = np.float(line[3])              # bump width (given as 6*sigma of the Gaussian)
    amp_bump_tmp = np.float(line[4])                 # bump height
    if i_int_bumpy==0 and amp_bump_tmp>0:       # remember (positive) topographical bump for correcting z_min later on
        amp_bump_topo = amp_bump_tmp

    if type_of_bumps[i_int_bumpy] == 1 :
       # convert Gaussian bumps stds into widths (input widths are given as 6*sigma)
       sigma_bumps[i_int_bumpy] = sigma6_bump_tmp / 6.

       # compute Gaussian amplitude coefficients in order to match the true heights given in input
       x_tmp = np.arange(-sigma_bumps[i_int_bumpy],sigma_bumps[i_int_bumpy],sigma_bumps[i_int_bumpy]/100)
       g_tmp = np.exp( -np.square(x_tmp) / (2*sigma_bumps[i_int_bumpy]**2) ) / np.sqrt(2*np.pi*sigma_bumps[i_int_bumpy]**2)
       amp_bumps[i_int_bumpy] = amp_bump_tmp / g_tmp.max()

    elif type_of_bumps[i_int_bumpy] == 2 :
       # for triangular bumps, things are simpler
       sigma_bumps[i_int_bumpy] = sigma6_bump_tmp
       amp_bumps[i_int_bumpy] = amp_bump_tmp

    else :
       print("Error: bump type %i not recognized (should be 1->Gaussian or 2->triangular)." % type_of_bumps[i_int_bumpy])
       exit("Error: bump type not recognized.")


# read nb of "wavy" interfaces
line = fid.readline().strip().split()
n_wavy = np.int_(line[0])
print("nb of wavy interfaces =",n_wavy)

# read properties of "wavy" interfaces
flag_wavy = np.zeros(n_int)    # are the interfaces "wavy"? (yes/no)
lambda_wavs = np.ones(n_int)   # wavelengths of the undulations
amp_wavs = np.zeros(n_int)     # amplitudes of the undulations
for i_wavy in range(0,n_wavy):
    line = fid.readline().strip().split()         # read new line in input file
    i_int_wavy = np.int_(line[0])                 # index of "wavy" interface
    flag_wavy[i_int_wavy] = 1                     # this interface is "wavy"
    lambda_wavs[i_int_wavy] = np.float(line[1])   # wavelength of the undulations
    amp_wavs[i_int_wavy] = np.float(line[2])      # amplitude of the undulations


# read nb of rectangular anomalies
line = fid.readline().strip().split()
n_ano = np.int_(line[0])
print("nb of anomalies =",n_ano)

# init. properties of anomalies
z0_ano = np.nan*np.zeros(n_ano)
dz_ano = np.nan*np.zeros(n_ano)
x0_ano = np.nan*np.zeros(n_ano)
dx_ano = np.nan*np.zeros(n_ano)
y0_ano = np.nan*np.zeros(n_ano)
dy_ano = np.nan*np.zeros(n_ano)
vp_ano = np.nan*np.zeros(n_ano)
vs_ano = np.nan*np.zeros(n_ano)
rho_ano= np.nan*np.zeros(n_ano)
qp_ano = np.nan*np.zeros(n_ano)
qs_ano = np.nan*np.zeros(n_ano)

for i_ano in range(0,n_ano):
    # read geometry of anomalies
    line = fid.readline().strip().split()   # read new line in input file
    z0_ano[i_ano] = np.float(line[0])       # z-location of anomaly (centre)
    dz_ano[i_ano] = np.float(line[1])       # z-width    "     "
    x0_ano[i_ano] = np.float(line[2])       # x-location "     "       "
    dx_ano[i_ano] = np.float(line[3])       # x-width    "     "

    # read properties of anomalies
    line = fid.readline().strip().split()   # read new line in input file
    vp_ano[i_ano] = np.float(line[0])       # vp of anomaly (m/s)
    vs_ano[i_ano] = np.float(line[1])       # vs  "    "    (m/s)
    rho_ano[i_ano]= np.float(line[2])       # rho "    "    (kg/m3)
    qp_ano[i_ano] = np.float(line[3])       # qp  "    "
    qs_ano[i_ano] = np.float(line[4])       # qs  "    "


# apply case-specific hard-coded tuning?
line = fid.readline().strip().split()   # read new line in input file
if np.int_(line[0]) == 0 :
   case_specific_tuning = False
elif np.int_(line[0]) == 1 :
   case_specific_tuning = True
   dam_version = 0   # (0->no water, 1->with water)
else :
   print("Error: option %i not recognized for case-specific tuning (should be 0->False or 1->True)." % line[0])
   exit("Error: unrecognized option for case-specific tuning.")

#-- end read input file


#-- adapt z_min in case of topography (i.e. non-flat 1st interface)
print("input zmin = %g m" % zmin)
zmin = zmin - amp_wavs[0] - amp_bump_topo
# FL: sometimes this is not accurate (e.g. if the baseline depth of the bumpy interface is not z=0),
#     so we need to hard-code it...
if case_specific_tuning and (dam_version >= 0) :
   zmin = 0.0
print("final zmin = %g m (with topography)" % zmin)

#-- build (default) coordinates vectors
vz = np.arange(zmin,zmax+0.1*dz,dz)
nz = len(vz)
vx = np.arange(xmin,xmax+0.1*dx,dx)
nx = len(vx)
# NB: "xmax+0.1*dx" is a trick to actually get values from xmin to xmax, included.

#-- init. 2D arrays
# (beware of the order, [nz,nx,ny], both for 2D/3D management and for compatibility with SEM array format)
vp = np.nan*np.zeros((nz,nx))
vs = np.nan*np.zeros((nz,nx))
rh = np.nan*np.zeros((nz,nx))
qp = np.nan*np.zeros((nz,nx))
qs = np.nan*np.zeros((nz,nx))
ztopo = np.nan*np.zeros((nx))

#-- Build model
for ix in range(0,len(vx)):
    xi = vx[ix]

    for iz in range(0,len(vz)):
        zi = vz[iz]

        #-- loop over interfaces/layers
        for i_int in range(0,n_int):

            #-- compute depth variation due to bumps
            if type_of_bumps[i_int] == 1 :
               # Gaussian bump
               z_bump = amp_bumps[i_int] * np.exp( -np.square(xi-x0_bumps[i_int]) / (2*sigma_bumps[i_int]**2) ) \
                                         / np.sqrt(2*np.pi*sigma_bumps[i_int]**2)
            elif type_of_bumps[i_int] == 2 :
               # triangular bump
               if abs(xi-x0_bumps[i_int]) > 0.5*sigma_bumps[i_int] :
                  z_bump = 0.
               else :
                  z_bump = amp_bumps[i_int] * ( 1. - 2*abs(xi-x0_bumps[i_int]) / sigma_bumps[i_int] )
            else :
               # no bump
               z_bump = 0.

            #-- compute depth of interface, taking dip, bumps and waves into account
            z_int = z_in[i_int] \
                  - xi * np.tan(np.pi*sl_in[i_int]/180.) \
                  + amp_wavs[i_int]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int] ) \
                  - z_bump

            #if i_int == 0 :
            #   # the 1st interface gives the topography of the model
            #   ztopo[ix] = z_int
            # FL, 07/01/20: topography is now defined as the first non-NaN vertical value for each x-point (see below)

            #-- also (re)compute the depth of all other interfaces in order to avoid intersections
            # interfaces above current one
            z_int_above = -np.inf   # init.
            for i_int2 in range(0,i_int):
                if type_of_bumps[i_int2] == 1 :
                   z_bump = amp_bumps[i_int2] * np.exp( -np.square(xi-x0_bumps[i_int2]) / (2*sigma_bumps[i_int2]**2) ) \
                                              / np.sqrt(2*np.pi*sigma_bumps[i_int2]**2)
                elif type_of_bumps[i_int2] == 2 :
                   if abs(xi-x0_bumps[i_int2]) > 0.5*sigma_bumps[i_int2] :
                      z_bump = 0.
                   else :
                      z_bump = amp_bumps[i_int2] * ( 1. - 2*abs(xi-x0_bumps[i_int2]) / sigma_bumps[i_int2] )
                else :
                   z_bump = 0.

                z_int_above_tmp = z_in[i_int2] \
                                - xi * np.tan(np.pi*sl_in[i_int2]/180.) \
                                + amp_wavs[i_int2]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int2] ) \
                                - z_bump
                z_int_above = max(z_int_above,z_int_above_tmp)

            # interfaces below current one
            z_int_below = np.inf   # init.
            for i_int2 in range(i_int+1,n_int-(i_int+1)):
                if type_of_bumps[i_int2] == 1 :
                   z_bump = amp_bumps[i_int2] * np.exp( -np.square(xi-x0_bumps[i_int2]) / (2*sigma_bumps[i_int2]**2) ) \
                                              / np.sqrt(2*np.pi*sigma_bumps[i_int2]**2)
                elif type_of_bumps[i_int2] == 2 :
                   if abs(xi-x0_bumps[i_int2]) > 0.5*sigma_bumps[i_int2] :
                      z_bump = 0.
                   else :
                      z_bump = amp_bumps[i_int2] * ( 1. - 2*abs(xi-x0_bumps[i_int2]) / sigma_bumps[i_int2] )
                else :
                   z_bump = 0.

                z_int_below_tmp = z_in[i_int2] \
                                - xi * np.tan(np.pi*sl_in[i_int+1]/180.) \
                                + amp_wavs[i_int2]  * np.cos( 2*np.pi*xi/lambda_wavs[i_int2] ) \
                                - z_bump
                z_int_below = min(z_int_below,z_int_below_tmp)

            # (manage dipping interfaces)
            if ( abs(sl_in[i_int])<1.e-3 and zi >= z_int) or \
               ( abs(sl_in[i_int])>1.e-3 and zi >= z_int and zi >= z_int_above and zi <= z_int_below ):

               if flag_grad_topo[i_int] == 0 :
                  # apply gradient wrt absolute depth
                  if abs(sl_in[i_int]) > 0.0 :
                     if ix == 0 and iz == 0 :
                        print("Warning: this is a dipping layer and you want to apply a gradient")
                        print("according to absolute depth: be aware that it is the input depth")
                        print("of the previous layer that is considered in this case.\n")
                     if i_int == 0 :
                        print("Error: no previous layer for defining absolute depth.")
                        exit("Error: no previous layer for defining absolute depth.")
                     z_grad = zi-z_in[i_int-1]
                  else :
                     z_grad = zi-z_in[i_int]
               elif flag_grad_topo[i_int] == 1 :
                  # apply gradient wrt depth below interface
                  z_grad = zi-z_int

               #-- assign layer properties
               vp[iz,ix] = vp_in[i_int] + vp_grad[i_int]*z_grad
               vs[iz,ix] = vs_in[i_int] + vs_grad[i_int]*z_grad
               rh[iz,ix] = rho_in[i_int] + rh_grad[i_int]*z_grad
               qp[iz,ix] = qp_in[i_int] + qp_grad[i_int]*z_grad
               qs[iz,ix] = qs_in[i_int] + qs_grad[i_int]*z_grad
               del z_grad


            if debug:
                if ( abs(sl_in[i_int])<1.e-3 and zi >= z_int):
                    fid_debug.write("interface nb %i is not dipping: simply assign value to [ix,iz] = [%i,%i] at (xi,zi) = (%g,%g)\n" \
                                    % (i_int,ix,iz,xi,zi) )
                elif ( abs(sl_in[i_int])>1.e-3 and zi >= z_int and zi >= z_int_above and zi <= z_int_below ):
                    fid_debug.write("interface nb %i is     dipping:        assign value to [ix,iz] = [%i,%i] at (xi, zi) = (%g,%g)\n" \
                                    % (i_int,ix,iz,xi,zi) )
                    fid_debug.write("          NB: depths of interfaces above/below = %g / %g\n" % (z_int_above,z_int_below) )

            if case_specific_tuning :
            # case-specific hard-coded tuning for the design of dams:
               #-1- do not put any water on the right-hand side of the dam
               if (dam_version == 1) and (i_int == 0) and (xi > x0_bumps[1]) :
                  vp[iz,ix] = np.nan
                  vs[iz,ix] = np.nan
                  rh[iz,ix] = np.nan
                  qp[iz,ix] = np.nan
                  qs[iz,ix] = np.nan
               #-2- truncate the dam to get a 10-m-wide flat top
               if (zi < 0.5*10.0*np.tan(np.pi*30./180.) ) :
                  vp[iz,ix] = np.nan
                  vs[iz,ix] = np.nan
                  rh[iz,ix] = np.nan
                  qp[iz,ix] = np.nan
                  qs[iz,ix] = np.nan

        #-- end loop over interfaces (for i_int)

        #-- loop over anomalies
        for i_ano in range(0,n_ano):
             #-- assign anomaly properties (overwrite layer properties)
             if xi>=x0_ano[i_ano]-0.5*dx_ano[i_ano] and xi<=x0_ano[i_ano]+0.5*dx_ano[i_ano] and \
                zi>=z0_ano[i_ano]-0.5*dz_ano[i_ano] and zi<=z0_ano[i_ano]+0.5*dz_ano[i_ano]:
                 vp[iz,ix] = vp_ano[i_ano]
                 vs[iz,ix] = vs_ano[i_ano]
                 rh[iz,ix] = rho_ano[i_ano]
                 qp[iz,ix] = qp_ano[i_ano]
                 qs[iz,ix] = qs_ano[i_ano]
        #-- end for i_ano

    #-- end for iz
#-- end for ix


#-- define topography as the first non-NaN vertical value for each x-point
for ix in range(0,len(vx)):
    xi = vx[ix]

    ztopo_is_defined_for_this_x = False

    if not np.isnan(vp[0,ix]) :
       ztopo[ix] = zmin
       ztopo_is_defined_for_this_x = True

    for iz in range(1,len(vz)):
        zi = vz[iz]

        if np.isnan(vp[iz-1,ix]) and (not np.isnan(vp[iz,ix])) :
           if ztopo_is_defined_for_this_x :
              print("Error: ztopo is already defined for x = %g m (z = %g m)." % (xi,ztopo[ix]))
              exit("Error: ztopo is already defined.")
           else :
              ztopo[ix] = zi
              ztopo_is_defined_for_this_x = True
    #-- end for iz

    if not ztopo_is_defined_for_this_x :
       print("Error: ztopo has not been defined for x = %g m (z = %g m)." % (xi,ztopo[ix]))
       exit("Error: ztopo has not been defined.")
#-- end for ix


#-- make output directory
if not os.path.isdir(dir_ou): os.makedirs(dir_ou)

#-- print model to file
file_ou = dir_ou + ("/vp_n%ix%i_h%gx%gm" % (nz,nx,dz,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(vp.flatten('F')), *vp.flatten('F')))
fid.close()

file_ou = dir_ou + ("/vs_n%ix%i_h%gx%gm" % (nz,nx,dz,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(vs.flatten('F')), *vs.flatten('F')))
fid.close()

file_ou = dir_ou + ("/rho_n%ix%i_h%gx%gm" % (nz,nx,dz,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(rh.flatten('F')), *rh.flatten('F')))
fid.close()

file_ou = dir_ou + ("/qp_n%ix%i_h%gx%gm" % (nz,nx,dz,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(qp.flatten('F')), *qp.flatten('F')))
fid.close()

file_ou = dir_ou + ("/qs_n%ix%i_h%gx%gm" % (nz,nx,dz,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(qs.flatten('F')), *qs.flatten('F')))
fid.close()


#-- print topo to file
file_ou = dir_ou + ("/ztopo_n%i_h%gm" % (nx,dx))
fid = open(file_ou + ".bin","wb")
fid.write(struct.pack('f'*len(ztopo.flatten('F')), *ztopo.flatten('F')))
fid.close()


#-- copy input file to output dir for further reference
shutil.copy(file_in, dir_ou)


#-- plot model
if plot_model:
    # init. fig
    fig = plt.figure(figsize=(wfig,hfig))
    ax = plt.gca()

    # plot model
    plot = plt.imshow(vp, extent=[xmin,xmax,zmin,zmax], origin='lower', cmap=cmap)

    # plot topo
    plt.plot(vx, ztopo, '--', color='black', linewidth=1.5)

    # axis
    plt.xlabel("x (m)")
    plt.ylabel("z (m)")
    ax.xaxis.tick_top()
    ax.xaxis.set_label_position('top')
    plt.gca().invert_yaxis()
    ax.axis('equal')

    # colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.25)
    cbar = plt.colorbar(plot,cax=cax,orientation='vertical')
    cbar.set_label("$V_P$ (m/s)")

    # save figure
    fig.savefig(file_ou_fig+'.pdf', format='pdf')

#-- end if plot_model


#-- close debug file
if debug:
    fid_debug.close()

