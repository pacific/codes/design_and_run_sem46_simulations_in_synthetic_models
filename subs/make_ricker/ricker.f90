! -----------------------------------------------------------------------------------------
! PROGRAM RICKER: BUILD A RICKER WAVELET
! INPUTS:
! N1: NUMBER OF SAMPLES
! D1: SAMPLING RATE IN SECONDS
! F0: MAIN FREQUENCY IN HZ
! OUTPUT: FRICKPERPP, FRICKERP, FRICKER, FRICKERD: DOUBLE PRIMITIVE, PRIMITIVE OF RICKER, RICKER, DERIVATIVE OF RICKER
! IN DIRACT ACCESS BINARY FILE
! -----------------------------------------------------------------------------------------
!
 IMPLICIT NONE

 REAL, ALLOCATABLE   :: x(:),dx(:),px(:),ppx(:)

 INTEGER 	:: n1,i1

 REAL     	:: d1,t0,f0,pi,da,t,a,a2      

 write(*,*) "NUMBER OF SAMPLES (NT) SAMPLING RATE (DT)  MAIN FREQUENCY (F0)"
 read(*,*) n1,d1,f0

 pi=3.141592654
 t0=1.5*sqrt(6.)/(pi*f0)

 open(8,file='frickerpp',access='direct',recl=n1*4)
 open(9,file='frickerp',access='direct',recl=n1*4)
 open(10,file='fricker',access='direct',recl=n1*4)
 open(11,file='frickerd',access='direct',recl=n1*4)

 da=pi*f0

 allocate (x(n1))
 allocate (dx(n1))
 allocate (px(n1))
 allocate (ppx(n1))

 do i1=1,n1
 t=float(i1-1)*d1
 a=pi*f0*(t-t0)
 a2=(pi*f0*(t-t0))**2
 ppx(i1)=-(0.5/(da*da))*exp(-a2)
 px(i1)=(a/da)*exp(-a2)
 x(i1)=(1.-2.*a2)*exp(-a2)
 dx(i1)=-4*a*da*exp(-a2)-2.*a*da*(1.-2.*a2)*exp(-a2)
 end do

 write(8,rec=1) ppx
 write(9,rec=1) px
 write(10,rec=1) x
 write(11,rec=1) dx

 close(8)
 close(9)
 close(10)
 close(11)

 deallocate (x)
 deallocate (dx)
 deallocate (px)
 deallocate (ppx)

 stop
 end
